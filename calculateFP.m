function FP = calculateFP(UInp,UOut,UInq,File,ExtIF,CFndx)
% calculateFP method implements the IFPUG Function point method algorithm. 
%
% Remark: 1) relative effort can be counted easily, because FP size-effort
% relation is linear.
%
% Complexity: Ordo(UInp + UOut + UInq + File + ExtIF)
%
% Syntax:
%   FP = calculateFP(UInp,UOut,UInq,File,ExtIF,CFndx)
%   Input params:
%       UInp    - Number of User Inputs in [simple,average,complex] vector
%       UOut    - Number of User Outputs in [simple,average,complex] vector
%       UInq    - Number of User Inquiries in [simple,average,complex] vector
%       File    - Number of Files in [simple,average,complex] vector
%       ExtIF   - Number of External Interfaces in [simple,average,complex] vector
%       CFndx   - Complexity Factor (CF) indeces
%   Return values:
%      FP       - calculated function point
%
% Reference:
%
%   Author(s): Akos Szoke
%   Copyright (c) 2006
%   $Revision: 1.0 $  $Date: 2007/10/05 15:41:44 $

% -- input checking --
% Complexity factor should not be empty
[r,c,v1] = find(CFndx < 1);
[r,c,v2] = find(CFndx > 6);
if ~isempty(v1) || ~isempty(v2)
    error('Complexity factor value is out of range!');
end;

% -- Constants --
UInpW  = [3,4,6];                              % User input weights
UOutW  = [4,5,7];                              % User output weights
UInqW  = [3,4,6];                              % User inquiries weights
FileW  = [7,10,15];                            % File weights
ExtIFW = [5,7,10];                             % External interfaces weights
CFW    = [0,1,2,3,4,5];                        % Complexity factor

% -- function body --

% Total Weighted Function Count (FC)
FC = sum(UInpW .* UInp) + ...
     sum(UOutW .* UOut) + ...
     sum(UInqW .* UInq) + ...
     sum(FileW .* File) + ...
     sum(ExtIFW .* ExtIF);

CF = sum(CFW(CFndx));            % Complexity factor (CF)
FP = FC * (0.65 + 0.01 * CF);    % Function point (FP)

% --- end of original FP ---

% end;