function [ADAP,SF,EDEM,PAEM,MAEM] = getCOC2Params()
% getCOC2Params returns the COCOMOII.2000 models parameters
%
% Complexity: Ordo(linear)
%
% Syntax:
%   SizeM = calculateCOC2Maint(AdaP,SUndx,UNFMndx,BSLOC,ADDSLOC,MODSLOC)
%   Input params:
%       -
%   Return values:
%       ADAP            - Adaptation parameter structure
%       SF              - Scaling factors
%       EDEM            - Early design effort multipliers
%       PAEM            - Post-architecture effort multipliers
%       MAEM            - Maintenence effort multipliers
%
% Reference:
% (Boehm:co2) 
%   Boehm, B. W.; Horowitz, E.; Madachy, R.; Reifer, D.; Clark, B. K.;
%   Steece, B.; Brown, W. A.; Chulani, S. & Abts, C. 
%   Software Cost Estimation with Cocomo II Prentice Hall PTR, 2000

%   Author(s): Akos Szoke
%   Copyright (c) 2006
%   $Revision: 1.0 $  $Date: 2007/10/05 15:41:44 $

% This file is part of PSched Toolbox.
% 

% -- function body --

% Adaptation parameters (3)
ADAP.AA     = [0.00 2.00 4.00 6.00 8.00];       % Percentage of assessment and assimilation
ADAP.SU     = [50.00 40.00 30.00 20.00 10.00];  % Percentage of software understanding
ADAP.UNFM   = [0.00 0.20 0.40 0.60 0.80 1.00];  % Programmer unfamiliarity with software

% Scaling factor parameters (5)
SF.PREC = [6.20 4.96 3.72 2.48 1.24 0.00];     % Precedentedness
SF.FLEX = [5.07 4.05 3.04 2.03 1.01 0.00];     % Development Flexibility
SF.RESL = [7.07 5.65 4.24 2.83 1.41 0.00];     % Architecture / Risk Resolution
SF.TEAM = [5.48 4.38 3.29 2.19 1.10 0.00];     % Team Cohesion
SF.PMAT = [7.80 6.24 4.68 3.12 1.56 0.00];     % Process Maturity

% Early design effort multiplier parameters (7)
EDEM.RCPX = [0.49 0.60 0.83 1.00 1.33 1.91 2.72];   % Product Reliability and Complexity
EDEM.RUSE = [0.95 1.00 1.07 1.15 1.24];             % Required Reusability
EDEM.PDIF = [1.00 1.00 1.00];                       % Platform Difficulty
EDEM.PERS = [2.12 1.62 1.26 1.00 0.83 0.63 0.50];   % Personnel Capability
EDEM.PREX = [1.59 1.33 1.12 1.00 0.87 0.74 0.62];   % Personnel Experience
EDEM.FCIL = [1.43 1.30 1.10 1.00 0.87 0.73 0.62];   % Facilities
EDEM.SCED = [1.43 1.14 1.00 1.00 1.00];             % Required Development Schedule

% Post-architecture effort multiplier parameters (17)
PAEM.RELY = [0.82 0.92 1.00 1.10 1.26];         % Required Software Reliability 
PAEM.DATA = [0.90 1.00 1.14 1.28];              % Data Base Size
PAEM.CPLX = [0.73 0.87 1.00 1.17 1.34 1.74];    % Product Complexity
PAEM.RUSE = [0.95 1.00 1.07 1.15 1.24];         % Required Reusability
PAEM.DOCU = [0.81 0.91 1.00 1.11 1.23];         % Documentation Match to Life-cycle Needs
PAEM.TIME = [1.00 1.11 1.29 1.63];              % Time Constraint
PAEM.STOR = [1.00 1.05 1.17 1.46];              % Storage Constraint
PAEM.PVOL = [0.87 1.00 1.15 1.30];              % Platform Volatility
PAEM.ACAP = [1.42 1.19 1.00 0.85 0.71];         % Analyst Capability
PAEM.PCAP = [1.34 1.15 1.00 0.88 0.76];         % Programmer Capability
PAEM.APEX = [1.22 1.10 1.00 0.88 0.81];         % Applications Experience
PAEM.PEXP = [1.19 1.09 1.00 0.91 0.85];         % Platform Experience
PAEM.LTEX = [1.20 1.09 1.00 0.91 0.84];         % Language and Tool Experience
PAEM.PCON = [1.29 1.12 1.00 0.90 0.81];         % Personnel Continuity
PAEM.TOOL = [1.17 1.09 1.00 0.90 0.78];         % Use of Software Tools
PAEM.SITE = [1.22 1.09 1.00 0.93 0.86 0.80];    % Multi-Site Development
PAEM.SCED = [1.43 1.14 1.00 1.00 1.00];         % Required Development Schedule

% Maintenance effort multiplier parameters (15)
MAEM.RELY = [1.23 1.10 1.00 0.99 1.07]; % Required Software Reliability  
MAEM.DATA = PAEM.DATA;                  % Data Base Size
MAEM.CPLX = PAEM.CPLX;                  % Product Complexity
MAEM.DOCU = PAEM.DOCU;                  % Documentation Match to Life-cycle Needs
MAEM.TIME = PAEM.TIME;                  % Time Constraint
MAEM.STOR = PAEM.STOR;                  % Storage Constraint
MAEM.PVOL = PAEM.PVOL;                  % Platform Volatility
MAEM.ACAP = PAEM.ACAP;                  % Analyst Capability
MAEM.PCAP = PAEM.PCAP;                  % Programmer Capability
MAEM.APEX = PAEM.APEX;                  % Applications Experience
MAEM.PEXP = PAEM.PEXP;                  % Platform Experience
MAEM.LTEX = PAEM.LTEX;                  % Language and Tool Experience
MAEM.PCON = PAEM.PCON;                  % Personnel Continuity
MAEM.TOOL = PAEM.TOOL;                  % Use of Software Tools
MAEM.SITE = PAEM.SITE;                  % Multi-Site Development

% end;
