%% SOFTWARE ESTIMATION TOOLBOX - SAMPLE TEST SET
% Software Estimation Toolbox contains algorithms to predict the most realistic use of effort required to develop or maintain software.
%
% *Author*: Akos Szoke (aszoke@mit.bme.hu)
%
% *Web site*: <http://www.kese.hu>
%
% *Toolbox Source*: <https://bitbucket.org/aszoke/>
%

%%
dbstop if error;
clear;

disp(' *********************************************** ');
disp(' *** SOFTWARE ESTIMATION TOOLBOX TEST: Start *** ')
disp(' *********************************************** ');

%% USE CASE POINT ESTIMATION METHOD
disp(' *** USE CASE POINT METHOD: Start *** ')
% example based on: Online bookstore example
%       
ac = [2,1,3];
uc = [1,2,2,1,1,1,3,1,1,1,1];
tp = [4,4,4,4,4,4,4,4,4,4,4,4,4];
ep = [4,4,4,4,4,4,4,4];
[ucp, twh, awh, uwh] = calculateUCP(ac,uc,tp,ep)

disp(' *** TEST: Passed *** ')

%% FUNCTION POINT ESTIMATION METHOD
disp(' *** FUNCTION POINT METHOD: Start *** ')
% example based on: -
%       
uinp  = [1,2,1];
uout  = [3,2,1];
uinq  = [2,2,2];
file  = [1,1,1];
extif = [1,2,3];
CFndx  = [4,4,4,4,4,4,4,4,4,4,4,4,4,4];
fp = calculateFP(uinp,uout,uinq,file,extif,CFndx)

disp(' *** TEST: Passed *** ')

%% COCOMOII.2000 ESTIMATION METHOD
disp(' *** COCOMOII.2000 ESTIMATION METHOD: Start *** ')
% example based on: -
%       

% -- common ED, PA, MA model parameters ---
SUndx   = 3;
UNFMndx = 3;
SFndx   = [3,3,3,3,3];
% Project parameters - these factors that must be CALIBRATED!
PRJP.A      = 2.94;     % productivity efficient
PRJP.B      = 0.91;     % base exponent
PRJP.C      = 3.67;     % schedule coefficient
PRJP.D      = 0.28;     % scaling base exponent
PRJP.ATPROD = 2.4;      % Automatic translation productivity (default = 2.4 KSLOC/PM)

% ===== New and reuse development size estimation ===== 
% -- common ED, PA model parameters ---
CM = 20;
DM = 20;
IM = 20;
AT = 50;
REVL = 0;
ASLOC = 0;
NSLOC = 100;
AAndx = 3;
SCEDp = 100;

% Early design model calculation
EDEMndx = [4,2,2,4,4,4,3];                          % ED model special parameters
[Size,PM,TDEV] = calculateCOC2DevReuse(CM,DM,IM,AAndx,SUndx,UNFMndx,AT,REVL,ASLOC,NSLOC,PRJP,SFndx,EDEMndx,SCEDp)

% Post-archiecture model calculation
PAEMndx = [3,2,3,2,3,1,1,2,3,3,3,3,3,3,3,3,3];      % PA model special parameters
[Size,PM,TDEV] = calculateCOC2DevReuse(CM,DM,IM,AAndx,SUndx,UNFMndx,AT,REVL,ASLOC,NSLOC,PRJP,SFndx,PAEMndx,SCEDp)

% ===== Maintenance size estimation ===== 
BSLOC = 100;
ADDSLOC = 5;
MODSLOC = 5;
EMndx = [3,2,3,3,1,1,2,3,3,3,3,3,3,3,3];            % MA model special parameters 
TM = 2;
[SizeM,PMM,FSPM] = calculateCOC2Maint(SUndx,UNFMndx,BSLOC,ADDSLOC,MODSLOC,PRJP,SFndx,EMndx,TM)

disp(' *** TEST: Passed *** ')

%%

disp(' *********************************************** ');
disp(' **** SOFTWARE ESTIMATION TOOLBOX TEST: End **** ')
disp(' *********************************************** ');

