function [UCP, TWH, ActorWH, UCaseWH] = calculateUCP(Actor,UCase,Tndx,Endx)
% calculateUCP method implements the Use case point method algorithm. 
%
% Remark: 1) relative effort can be counted easily, because UCPM size-effort
% relation is linear
%
%% Complexity: Ordo(UCase + Actor)
%
%% Syntax:
%   [UCP,TWH] = calculateUCP(Actor,UCase,Tndx,Endx)
%   Input params:
%       Actor    - Number of Actors in [simple,average,complex] vector
%       UCase    - Number of UseCases in [simple,average,complex] vector
%               its values are: simple/average/complex -> 1/2/3
%       Tndx    - technical complexity index
%       Endx    - environmental complexity index
%               its values are: irrelevant/.../essential -> [1,5]
%   Return values:
%      UCP      - calculated use case point                     - Size
%      TWH      - calculated effort in total working hour (TWH) - Effort
%      ActorWH  - Actor realization effort in working hour      - rel. effort
%      UCaseWH  - UseCase realization effort in working hour    - rel. effort 
%
%% Reference:
% (Anda:imp) 
%   Anda, B.; Angelvik, E. & Ribu, K. Improving
%   Estimation Practices by Applying Use Case Models 
%   PROFES '02: Proceedings of the 4th International Conference on Product Focused Software Process Improvement, 
%   Springer-Verlag, 2002, 383-397

%%   Author(s): Akos Szoke
%   Copyright (c) 2006
%   $Revision: 1.0 $  $Date: 2007/10/05 15:41:44 $

% -- input checking --
% Actor and UCase should not be empty
if isempty(Actor) || isempty(UCase) 
    error('Actor or Use case complexity list is empty!');
end;
% Actor and UCase should not be empty
[r,c,v1] = find(Actor < 1);
[r,c,v2] = find(Actor > 3);
[r,c,v3] = find(UCase < 1);
[r,c,v4] = find(UCase > 3);
if ~isempty(v1) || ~isempty(v2) || ~isempty(v3) || ~isempty(v4) 
    error('Actor or Use case complexity value is out of range!');
end;

% Tndx and Endx should not be empty
[r,c,v1] = find(Tndx < 1);
[r,c,v2] = find(Tndx > 6);
[r,c,v3] = find(Endx < 1);
[r,c,v4] = find(Endx > 6);
if ~isempty(v1) || ~isempty(v2) || ~isempty(v3) || ~isempty(v4) 
    error('Technical or Environmental property value is out of range!');
end;

% -- Constants -- 
% REMARK: TFW's second param is 2 in Anda's article!
ACW = [1,2,3];                              % Actor weights
UCW = [5,10,15];                            % Use case weights
TFW = [2,1,1,1,1,0.5,0.5,2,1,1,1,1,1];      % TFactor weights (13 factors)
EFW = [1.5,0.5,1,0.5,1,2,-1,-1];            % EFactor weights (8 factors)
WHPU = 20;                                  % Working hour per use case
CFEF = [0,1,2,3,4,5];                       % Complexity and Environmental factor

% -- function body --

% unadjusted actor weight (UAW)
UAW = 0;
for i = 1:size(Actor,2)
    UAW = UAW + ACW(Actor(i));
end;

% unadjusted use case weight (UUCW)
UUCW = 0;
for i = 1:size(UCase,2);
    UUCW = UUCW + UCW(UCase(i));
end;

UUCP = UAW + UUCW;              % unadjusted use case point (UUCP)

TFactor = sum(CFEF(Tndx) .* TFW);    
EFactor = sum(CFEF(Endx) .* EFW);    
TCF = 0.6 + (0.01 * TFactor);    % technical complexity factor (TCF)
EF = 1.4 + (-0.03 * EFactor);   % environmental factor (EF)

UCP = UUCP * TCF * EF;          % adjusted use case point
TWH = UCP * WHPU;               % total working hour

% --- end of original UCPM ---
% calculating the effort per Actors and UseCases
ActorUCP = ACW(Actor) * TCF * EF;
ActorWH = ActorUCP * WHPU;
UCaseUCP = UCW(UCase) * TCF * EF; 
UCaseWH = UCaseUCP * WHPU;

% end;