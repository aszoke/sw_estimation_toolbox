function [SizeM,PMM,FSPM] = calculateCOC2Maint(SUndx,UNFMndx,BSLOC,ADDSLOC,MODSLOC,PRJP,SFndx,EMndx,TM)
% calculateUCP method implements Early design and Post-architecture model computation of 
% COCOMOII.2000 in maintenance situation 
%
% Remark: 1) ...
%
% Complexity: Ordo(linear)
%
% Syntax:
%   SizeM = calculateCOC2Maint(AdaP,SUndx,UNFMndx,BSLOC,ADDSLOC,MODSLOC)
%   Input params:
%       SUndx       - Percentage of software understanding [1,5]
%       UNFMndx     - Programmer unfamiliarity with software [1,6]
%       BSLOC       - Base size KSLOC
%       ADDSLOC     - Added size KSLOC
%       MODSLOC     - Modified size KSLOC
%       PRJP        - Project parameters
%       SFndx       - Scaling factor indeces (5)
%       EMndx       - Maintenance effort multipliers
%       TM          - Fixed time for maintenance
%   Return values:
%       SizeM       - calculated size in KSLOC                      - Size
%       PMM         - Person month maintenance                      - Effort
%       FSPM        - Full Time Software Personnel for Maintenance  - Person
%
% Remark: relative size can NOT be counted easily, because reuse is non-linear.
%
% Reference:
% (Boehm:co2) 
%   Boehm, B. W.; Horowitz, E.; Madachy, R.; Reifer, D.; Clark, B. K.;
%   Steece, B.; Brown, W. A.; Chulani, S. & Abts, C. 
%   Software Cost Estimation with Cocomo II Prentice Hall PTR, 2000

%   Author(s): Akos Szoke
%   Copyright (c) 2006
%   $Revision: 1.0 $  $Date: 2007/10/05 15:41:44 $

% -- input checking --
% FIXME:

% -- function body --

% get COCOMOII.2000 parameters
[ADAP,SF,EDEM,PAEM,MAEM] = getCOC2Params();

MAF = 1 + (ADAP.SU(SUndx)/100 * ADAP.UNFM(UNFMndx));  % Maintenance adjustment factor

MCF = (ADDSLOC + MODSLOC)/BSLOC;            % Maintenance change factor

SizeM = BSLOC * MCF * MAF;                  % Maintenance size

% Scaling exponent for effort
SummaSF =   SF.PREC(SFndx(1)) + ...
            SF.FLEX(SFndx(2)) + ...
            SF.RESL(SFndx(3)) + ...
            SF.TEAM(SFndx(4)) + ...
            SF.PMAT(SFndx(5));
E = PRJP.B + 0.01 * SummaSF;

% Nominal person months 
PMnomM = PRJP.A * SizeM^E;

% Effort adjustment factor /MA, effort estimation/
%   - without SCED,RUSE factors + modified RELY factor
EAFe =  MAEM.RELY(EMndx(1)) * ...
        MAEM.DATA(EMndx(2)) * ...
        MAEM.CPLX(EMndx(3)) * ...
        MAEM.DOCU(EMndx(4)) * ...
        MAEM.TIME(EMndx(5)) * ...
        MAEM.STOR(EMndx(6)) * ...
        MAEM.PVOL(EMndx(7)) * ...
        MAEM.ACAP(EMndx(8)) * ...
        MAEM.PCAP(EMndx(9)) * ...
        MAEM.APEX(EMndx(10)) * ...
        MAEM.PEXP(EMndx(11)) * ...
        MAEM.LTEX(EMndx(12)) * ...
        MAEM.PCON(EMndx(13)) * ...
        MAEM.TOOL(EMndx(14)) * ...
        MAEM.SITE(EMndx(15));

PMM = PMnomM * EAFe;    % Person month maintenance
FSPM = PMM/TM;          % Full Time Software Personnel for Maintenance

% end;