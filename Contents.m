% # Software Estimation Toolbox
%
% ## About
%
% Software Estimation Toolbox contains algorithms to predict the most realistic use of effort required to develop or maintain software.
%
% **Author**: Akos Szoke <aszoke@mit.bme.hu>
%
% **Web site**: [http://www.kese.hu](http://www.kese.hu)
%
% **Toolbox Source**: [https://bitbucket.org/aszoke/](https://bitbucket.org/aszoke/)
% 
% ### Features
% 
% * **calculateCOC2DevReuse** - implements Early design and Post-architecture model computation of COCOMOII.2000 in new development and reuse situation 
% * **calculateCOC2Maint** - implements Early design and Post-architecture model computation of COCOMOII.2000 in maintenance situation 
% * **calculateFP** - implements the IFPUG Function point method algorithm 
% * **calculateUCP** - implements the Use case point method algorithm
%
% ### Dependencies
%
% -
%
% ## Usage
% 
% using the functions in the feature list
%
% ### Example
% 
% [test_sw_estimation_toolbox.m](./sw_estimation_toolbox/src/master/test_sw_estimation_toolbox.m)
% 
% The generated log of the example can be found in
% [test_sw_estimation_toolbox.html](https://bitbucket.org/aszoke/sw_estimation_toolbox/raw/master/html/test_sw_estimation_toolbox.html).
%
% **Note**: *Due to the Bitbucket HTML preview restriction (see [HTML rendering for generated doc](https://bitbucket.org/site/master/issue/6353/html-rendering-for-generated-doc)), in order to view the Test run samples in HTML, use should use the [GitHub & BitBucket HTML Preview](http://htmlpreview.github.io/) service. (Copy the link below and paste into the text box that can be found in the service.)*
%
% ### Test data
% 
% see them in the example
%
% ## License
%
% (The MIT License)
%
% Copyright (c) 2011 Akos Szoke
%
% Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

